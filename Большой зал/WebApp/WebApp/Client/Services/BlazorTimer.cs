﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
namespace WebApp.Client.Services
{
    public class BlazorTimer:IDisposable
    {
        private Timer _timer;
        public void SetTimer(double range,bool flag)
        {
            _timer = new Timer(range);
            _timer.Elapsed += (s,e) =>
            {
               

                OnElapsed?.Invoke();
                _timer.Dispose();
            };
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        public event Action OnElapsed;
       
    }
}
