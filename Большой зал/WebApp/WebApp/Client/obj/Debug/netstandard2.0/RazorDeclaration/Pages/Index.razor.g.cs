#pragma checksum "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b5936abba3eb68fe0a0ceaf70a1365850d631366"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace WebApp.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 2 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 3 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 4 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 5 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 6 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using WebApp.Client;

#line default
#line hidden
#line 7 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using WebApp.Client.Shared;

#line default
#line hidden
#line 2 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\Index.razor"
using WebApp.Shared;

#line default
#line hidden
#line 3 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\Index.razor"
using System.Threading;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#line 71 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\Index.razor"
      
    IEnumerable<ViscaCamera> cameras = new List<ViscaCamera>();
    IEnumerable<KramerDevice> kramers = new List<KramerDevice>();
    IEnumerable<BoschInfo> dicentis = new List<BoschInfo>();
    protected override async Task OnInitializedAsync()
    {
        try
        {
            cameras = await Http.GetJsonAsync<List<ViscaCamera>>("api/Cameras");
            kramers = await Http.GetJsonAsync<List<KramerDevice>>("api/KramerDevices");
            dicentis = await Http.GetJsonAsync<List<BoschInfo>>("api/BoschInfoes");
            await Http.PostJsonAsync("api/Dicentis/setup",dicentis.ElementAt(0));
            var answer = await Http.GetJsonAsync<bool>("api/Dicentis/isLogged");
            Console.WriteLine(answer);
            if (!answer) 
            {
                var credentials = await Http.GetJsonAsync<BoschInfo>("api/BoschInfoes/1"); 
                await Http.PostJsonAsync("api/Dicentis/login",dicentis.ElementAt(0));
            }
        }
        catch(Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    private async void Off()
    {
        await Http.PutJsonAsync("api/Dicentis/power",new EnergyStatus { State = 2 });
    }
    private async void On()
    {
        await Http.PutJsonAsync("api/Dicentis/power",new EnergyStatus { State = 0 });
    }
    

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
