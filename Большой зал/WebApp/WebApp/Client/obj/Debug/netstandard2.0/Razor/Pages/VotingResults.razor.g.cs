#pragma checksum "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0cbeb194fe4deab99a53cbb00f0cc4a18817e9bc"
// <auto-generated/>
#pragma warning disable 1591
namespace WebApp.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 2 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 3 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 4 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 5 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 6 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using WebApp.Client;

#line default
#line hidden
#line 7 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using WebApp.Client.Shared;

#line default
#line hidden
#line 2 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
using WebApp.Shared;

#line default
#line hidden
#line 3 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
using ChartJs.Blazor.Charts;

#line default
#line hidden
#line 4 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
using ChartJs.Blazor.ChartJS.PieChart;

#line default
#line hidden
#line 5 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
using ChartJs.Blazor.ChartJS.Common.Properties;

#line default
#line hidden
#line 6 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
using ChartJs.Blazor.Util;

#line default
#line hidden
#line 7 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
using System.Linq;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.RouteAttribute("/voting/results")]
    public partial class VotingResults : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<ol class=""breadcrumb"">
        <li class=""breadcrumb-item""><a href> Устройства </a> </li>
        <li class=""breadcrumb-item""> <a href=""/conference""> Конференция </a></li>
        <li class=""breadcrumb-item""> <a href=""/voting/setup""> Управление голосованием </a> </li>
        <li class=""breadcrumb-item active"">  Результат голосования  </li>
    </ol>
");
#line 16 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
 if(inited)
{

#line default
#line hidden
            __builder.AddContent(1, "    ");
            __builder.OpenComponent<ChartJs.Blazor.Charts.ChartJsPieChart>(2);
            __builder.AddAttribute(3, "Config", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<ChartJs.Blazor.ChartJS.PieChart.PieConfig>(
#line 18 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
                                                 _config

#line default
#line hidden
            ));
            __builder.AddAttribute(4, "Width", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#line 18 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
                                                                 600

#line default
#line hidden
            ));
            __builder.AddAttribute(5, "Height", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#line 18 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
                                                                              300

#line default
#line hidden
            ));
            __builder.AddComponentReferenceCapture(6, (__value) => {
#line 18 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
                           _pieChartJs = (ChartJs.Blazor.Charts.ChartJsPieChart)__value;

#line default
#line hidden
            }
            );
            __builder.CloseComponent();
            __builder.AddMarkupContent(7, "\r\n");
#line 19 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
#line 20 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\VotingResults.razor"
       
    bool inited = false;
    private PieConfig _config;
    private ChartJsPieChart _pieChartJs;
    private WebApp.Shared.VotingResults results = new WebApp.Shared.VotingResults();
    protected override async Task OnInitializedAsync()
    {
        var answer = await Http.GetJsonAsync<bool>("api/Dicentis/isLogged");
        Console.WriteLine(answer);
        if (!answer) 
        {
            var credentials = await Http.GetJsonAsync<BoschInfo>("api/BoschInfoes/1");
            await Http.PostJsonAsync("api/Dicentis/login",credentials);
        }
        results = await Http.GetJsonAsync<WebApp.Shared.VotingResults>("api/Dicentis/voting/results");
        _config = new PieConfig
        {
            Options = new PieOptions
            {
                Title = new OptionsTitle
                {
                    Display = true,
                    Text = "Результаты голосования"

                },
                Responsive = true,
                Animation = new ArcAnimation
                {
                    AnimateRotate = true,
                    AnimateScale = true
                }
            }
        };
        _config.Data.Labels.AddRange(results.Results.Select(res => res.Name).ToArray<string>());
        var pieSet = new PieDataset
        {
            BackgroundColor = new[] { ColorUtil.RandomColorString(), ColorUtil.RandomColorString(), ColorUtil.RandomColorString(), ColorUtil.RandomColorString() },
            BorderWidth = 0,
            HoverBackgroundColor = ColorUtil.RandomColorString(),
            HoverBorderColor = ColorUtil.RandomColorString(),
            HoverBorderWidth = 1,
            BorderColor = "#ffffff",
        };
        pieSet.Data.AddRange(results.Results.Select(res => (double)res.Value).ToArray<double>());
        _config.Data.Datasets.Add(pieSet);
        inited = true;



    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
