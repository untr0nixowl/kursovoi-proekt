#pragma checksum "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7f595d6f59f74e145476ae05a94eb948b5c306d4"
// <auto-generated/>
#pragma warning disable 1591
namespace WebApp.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 2 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 3 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 4 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 5 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 6 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using WebApp.Client;

#line default
#line hidden
#line 7 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\_Imports.razor"
using WebApp.Client.Shared;

#line default
#line hidden
#line 2 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
using WebApp.Shared;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.RouteAttribute("/tvs")]
    public partial class TVControl : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h4> Телевизоры </h4>\r\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "row");
            __builder.AddMarkupContent(3, "\r\n");
#line 7 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
     foreach(var tv in tvInfoes) 
    { 

#line default
#line hidden
            __builder.AddContent(4, "    ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "col-md-3 mb-4");
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.OpenElement(8, "div");
            __builder.AddAttribute(9, "class", "card shadow-lg  mb-3");
            __builder.AddAttribute(10, "style", "max-width: 18rem;");
            __builder.AddMarkupContent(11, "\r\n            ");
            __builder.OpenElement(12, "div");
            __builder.AddAttribute(13, "class", "card-header");
            __builder.AddContent(14, " ");
            __builder.AddContent(15, 
#line 11 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                       tv.Tv.Name

#line default
#line hidden
            );
            __builder.AddContent(16, " ");
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n            ");
            __builder.OpenElement(18, "div");
            __builder.AddAttribute(19, "class", "card-body");
            __builder.AddMarkupContent(20, "\r\n                ");
            __builder.OpenElement(21, "h5");
            __builder.AddAttribute(22, "class", "card-title");
            __builder.AddContent(23, " ");
            __builder.AddMarkupContent(24, "<em>Status </em> ");
            __builder.AddContent(25, 
#line 13 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                                           tv.State == 1 ? "Выключено" : tv.State == 2 ? "Включено" : "Не известно"

#line default
#line hidden
            );
            __builder.AddContent(26, " ");
            __builder.CloseElement();
            __builder.AddMarkupContent(27, "\r\n                ");
            __builder.OpenElement(28, "h5");
            __builder.AddAttribute(29, "class", "card-title");
            __builder.AddMarkupContent(30, "<em>IP: </em> ");
            __builder.AddContent(31, 
#line 14 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                                      tv.Tv.Ip

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(32, "\r\n                ");
            __builder.OpenElement(33, "h5");
            __builder.AddAttribute(34, "class", "card-title");
            __builder.AddMarkupContent(35, "<em>Port: </em> ");
            __builder.AddContent(36, 
#line 15 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                                        tv.Tv.Port

#line default
#line hidden
            );
            __builder.AddContent(37, " ");
            __builder.CloseElement();
            __builder.AddMarkupContent(38, "\r\n                ");
            __builder.OpenElement(39, "button");
            __builder.AddAttribute(40, "class", " btn" + " " + (
#line 16 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                      tv.State == 1? "btn-primary": "btn-outline-primary"

#line default
#line hidden
            ) + " btn-block");
            __builder.AddAttribute(41, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 16 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                                                                                                () => { Off(tv.Tv.Name); tv.State = 1;this.StateHasChanged();  }

#line default
#line hidden
            ));
            __builder.AddContent(42, " Off ");
            __builder.CloseElement();
            __builder.AddMarkupContent(43, "\r\n                ");
            __builder.OpenElement(44, "button");
            __builder.AddAttribute(45, "class", " btn" + " " + (
#line 17 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                      tv.State == 2? "btn-primary": "btn-outline-primary"

#line default
#line hidden
            ) + " btn-block");
            __builder.AddAttribute(46, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 17 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                                                                                                                () => { On(tv.Tv.Name); tv.State = 2;this.StateHasChanged(); }

#line default
#line hidden
            ));
            __builder.AddContent(47, " On ");
            __builder.CloseElement();
            __builder.AddMarkupContent(48, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(49, "\r\n            ");
            __builder.OpenElement(50, "div");
            __builder.AddAttribute(51, "class", "card-body bg-light");
            __builder.AddMarkupContent(52, "\r\n                ");
            __builder.OpenElement(53, "a");
            __builder.AddAttribute(54, "href", 
#line 20 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
                          "configuration/tv/" + tv.Tv.Id

#line default
#line hidden
            );
            __builder.AddAttribute(55, "class", "card-link");
            __builder.AddMarkupContent(56, " Настройки  ");
            __builder.CloseElement();
            __builder.AddMarkupContent(57, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(58, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(59, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(60, "\r\n");
#line 24 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
    }

#line default
#line hidden
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#line 28 "C:\Users\Untr0nix\dev\Work\MCKO\LargeHall\WebApp\WebApp\Client\Pages\TVControl.razor"
       
    private List<DisplayTVState> tvInfoes = new List<DisplayTVState>();
    protected override async Task OnInitializedAsync()
    {
        var tvs = await Http.GetJsonAsync<List<TvCredentials>>("api/TvCredentials");
        foreach(var tv in tvs)
        {
            tv.Name = tv.Name.Replace(' ', '-');
            Console.WriteLine(tv.Ip);
            await Http.PostJsonAsync("api/TV",tv);
            var tvInfo = new DisplayTVState { Tv = tv };
            tvInfo.State = await Http.GetJsonAsync<int>($"api/TV/{tv.Name}/status");
            tvInfoes.Add(tvInfo);
        }

    }
    private async void On(string name)
    {
        await Http.GetAsync($"api/TV/{name}/on");
    }
    private async void Off(string name)
    {
        await Http.GetAsync($"api/TV/{name}/off");
    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
