﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Server.Data;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BoschInfoesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public BoschInfoesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/BoschInfoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BoschInfo>>> GetBoschInfo()
        {
            return await _context.BoschInfo.ToListAsync();
        }

        // GET: api/BoschInfoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BoschInfo>> GetBoschInfo(int id)
        {
            var boschInfo = await _context.BoschInfo.FindAsync(id);

            if (boschInfo == null)
            {
                return NotFound();
            }

            return boschInfo;
        }

        // PUT: api/BoschInfoes/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBoschInfo(int id, BoschInfo boschInfo)
        {
            if (id != boschInfo.Id)
            {
                return BadRequest();
            }

            _context.Entry(boschInfo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BoschInfoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BoschInfoes
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<BoschInfo>> PostBoschInfo(BoschInfo boschInfo)
        {
            _context.BoschInfo.Add(boschInfo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBoschInfo", new { id = boschInfo.Id }, boschInfo);
        }

        // DELETE: api/BoschInfoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<BoschInfo>> DeleteBoschInfo(int id)
        {
            var boschInfo = await _context.BoschInfo.FindAsync(id);
            if (boschInfo == null)
            {
                return NotFound();
            }

            _context.BoschInfo.Remove(boschInfo);
            await _context.SaveChangesAsync();

            return boschInfo;
        }

        private bool BoschInfoExists(int id)
        {
            return _context.BoschInfo.Any(e => e.Id == id);
        }
    }
}
