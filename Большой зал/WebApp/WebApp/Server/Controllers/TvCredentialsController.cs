﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Server.Data;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TvCredentialsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TvCredentialsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TvCredentials
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TvCredentials>>> GetTvInfoes()
        {
            return await _context.TvInfoes.ToListAsync();
        }

        // GET: api/TvCredentials/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TvCredentials>> GetTvCredentials(int id)
        {
            var tvCredentials = await _context.TvInfoes.FindAsync(id);

            if (tvCredentials == null)
            {
                return NotFound();
            }

            return tvCredentials;
        }

        // PUT: api/TvCredentials/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTvCredentials(int id, TvCredentials tvCredentials)
        {
            if (id != tvCredentials.Id)
            {
                return BadRequest();
            }

            _context.Entry(tvCredentials).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TvCredentialsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TvCredentials
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TvCredentials>> PostTvCredentials(TvCredentials tvCredentials)
        {
            _context.TvInfoes.Add(tvCredentials);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTvCredentials", new { id = tvCredentials.Id }, tvCredentials);
        }

        // DELETE: api/TvCredentials/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TvCredentials>> DeleteTvCredentials(int id)
        {
            var tvCredentials = await _context.TvInfoes.FindAsync(id);
            if (tvCredentials == null)
            {
                return NotFound();
            }

            _context.TvInfoes.Remove(tvCredentials);
            await _context.SaveChangesAsync();

            return tvCredentials;
        }

        private bool TvCredentialsExists(int id)
        {
            return _context.TvInfoes.Any(e => e.Id == id);
        }
    }
}
