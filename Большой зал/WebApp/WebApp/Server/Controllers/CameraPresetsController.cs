﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Server.Data;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CameraPresetsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CameraPresetsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CameraPresets
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CameraPreset>>> GetCameraPresets()
        {
            return await _context.CameraPresets.ToListAsync();
        }

        // GET: api/CameraPresets/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CameraPreset>> GetCameraPreset(int id)
        {
            var cameraPreset = await _context.CameraPresets.FindAsync(id);

            if (cameraPreset == null)
            {
                return NotFound();
            }

            return cameraPreset;
        }
        [HttpGet("com/{comPort}")]
        public async Task<ActionResult<List<CameraPreset>>> GetCameraPreset(string comPort)
        {

            return await _context.CameraPresets.AsNoTracking().Where(cameraPreset => cameraPreset.ComPort == comPort).ToListAsync<CameraPreset>();
        }

        // PUT: api/CameraPresets/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCameraPreset(int id, CameraPreset cameraPreset)
        {
            if (id != cameraPreset.Id)
            {
                return BadRequest();
            }

            _context.Entry(cameraPreset).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CameraPresetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpGet("length")]
        public  ActionResult<int> GetLength()
        {
            return _context.CameraPresets.Count();
        }

        // POST: api/CameraPresets
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CameraPreset>> PostCameraPreset([FromBody] CameraPreset cameraPreset)
        {
            _context.CameraPresets.Add(cameraPreset);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCameraPreset", new { id = cameraPreset.Id }, cameraPreset);
        }

        // DELETE: api/CameraPresets/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CameraPreset>> DeleteCameraPreset(int id)
        {
            var cameraPreset = await _context.CameraPresets.FindAsync(id);
            if (cameraPreset == null)
            {
                return NotFound();
            }

            _context.CameraPresets.Remove(cameraPreset);
            await _context.SaveChangesAsync();

            return cameraPreset;
        }

        private bool CameraPresetExists(int id)
        {
            return _context.CameraPresets.Any(e => e.Id == id);
        }
    }
}
