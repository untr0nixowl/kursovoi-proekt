﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Kramers.Protocol;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.Server.Services;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KramersController : ControllerBase
    {
     
        private readonly IServerService _service;
        private readonly KramerService _kramer;
        public KramersController(IServerService service,KramerService kramer)
        {
             _kramer = kramer;
            _service = service;
        }
      
        [HttpGet]
        public async Task<IEnumerable<KramerDevice>> GetKramers()
        {
            return await _service.GetKramerDevices();
        }
        // POST: api/Kramers
        [HttpPost("kramer")]
        public void Post([FromBody] SwitchRequest value)
        {
            //kramer88.VID(new Kramers.RequestModel.SwitcherRequest { Path = new Kramers.Models.SwitchPath { InPort = value.In,OutPort = value.Out }, Request = false });
            _kramer.Switch(value);
        }

        
        [HttpPost("setup/kramer")]
        public void SetupKramerE([FromBody] KramerDevice value)
        {
            _kramer.SetupKramer(value);
        }

        [HttpGet("logout")]
        public void Logout()
        {
            _kramer.Logout();
        }
    }
}
