﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.Server.Services;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CamerasController : ControllerBase
    {
        // GET: api/Cameras
        private readonly IViscaService _service;
        private IServerService _server;
        public CamerasController(IViscaService service,IServerService server)
        {
            _service = service;
            _server = server;
        }
        
        [HttpGet]
        public  IEnumerable<ViscaCamera> Get()
        {
            return _service.GetViscaCameras();
        }

        // GET: api/Cameras/5
        [HttpGet("{comPort}")]
        public  ViscaCamera Get(string comPort)
        {
            return _service.GetViscaCamera(comPort);
        }

        // POST: api/Cameras
        [HttpPost("movement")]
        public void PostCameraAbsolute([FromBody] CameraAbsolute body)
        {
            _service.AbsoluteCamera(body);
        }

        // PUT: api/Cameras/5
        [HttpPut("movement/{id}")]
        public void PutCameraJog(int id, [FromBody] CameraJoystic body)
        {
             _service.JogCamera(id,body);
        }

        [HttpGet("movement/stop/{id}")]
        public void GetCameraStop(int id)
        {
             _service.CameraStop(id);
        }
        [HttpPut("zoom/{id}")]
        public void PutCameraZoom(int id,[FromBody] CameraZoom zoom)
        {
             _service.CameraZoom(id,zoom);
        }
        [HttpPut("zoom/stop/{id}")]
        public void PutCameraZoomStop(int id)
        {
             _service.CameraZoomStop(id);
        }
        [HttpGet("test")]
        public async Task<string> GetTest()
        {
            return await _server.Test();
        }
    }
}
