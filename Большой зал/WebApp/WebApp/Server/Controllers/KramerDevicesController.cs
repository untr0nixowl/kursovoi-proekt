﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Server.Data;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KramerDevicesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public KramerDevicesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/KramerDevices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<KramerDevice>>> GetKramers()
        {
            return await _context.Kramers.ToListAsync();
        }

        // GET: api/KramerDevices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<KramerDevice>> GetKramerDevice(int id)
        {
            var kramerDevice = await _context.Kramers.FindAsync(id);

            if (kramerDevice == null)
            {
                return NotFound();
            }

            return kramerDevice;
        }

        // PUT: api/KramerDevices/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutKramerDevice(int id, KramerDevice kramerDevice)
        {
            if (id != kramerDevice.Id)
            {
                return BadRequest();
            }

            _context.Entry(kramerDevice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KramerDeviceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/KramerDevices
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<KramerDevice>> PostKramerDevice(KramerDevice kramerDevice)
        {
            _context.Kramers.Add(kramerDevice);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKramerDevice", new { id = kramerDevice.Id }, kramerDevice);
        }

        // DELETE: api/KramerDevices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<KramerDevice>> DeleteKramerDevice(int id)
        {
            var kramerDevice = await _context.Kramers.FindAsync(id);
            if (kramerDevice == null)
            {
                return NotFound();
            }

            _context.Kramers.Remove(kramerDevice);
            await _context.SaveChangesAsync();

            return kramerDevice;
        }

        private bool KramerDeviceExists(int id)
        {
            return _context.Kramers.Any(e => e.Id == id);
        }
    }
}
