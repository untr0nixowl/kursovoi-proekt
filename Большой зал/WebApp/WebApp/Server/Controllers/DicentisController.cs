﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.Server.Services;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DicentisController : ControllerBase
    {
        private readonly IDicentisService _service;
        public DicentisController(IDicentisService service)
        {
            _service = service;
        }
        [HttpGet("isLogged")]
        public async Task<bool> IsLogged()
        {
            try
            {
                await _service.KeepAlive();
                return true;
            }
            catch
            {
                return false;
            }
        }
        // GET: api/Dicentis
        [HttpGet("seats")]
        public async  Task<IEnumerable<SeatEntry>> GetSeats()
        {
            return await _service.GetSeatEntriesAsync();
        }

        [HttpGet("speakers/{isPolling}")]
        public async  Task<IEnumerable<SpkEntry>> GetSpeakers(bool isPolling = false)
        {
            return await _service.GetSpkEntriesAsync(isPolling);
        }
        [HttpGet("waiting-list")]
        public async Task<IEnumerable<RtsEntry>> GetWaitings()
        {
            return await _service.GetWaitersAsync();
        }
        // POST: api/Dicentis
        [HttpPost("login")]
        public async Task Post([FromBody] BoschInfo value)
        {
            
            await _service.LoginAsync(value);
        }
        [HttpPost("setup")]
        public void Setup([FromBody]BoschInfo value)
        {
            _service.Setup(value.Ip);
        }
        [HttpPost("speakers")]
        public async Task Post([FromBody] PostSpeakerRequest value)
        {
            await _service.PostSpeakerAsync(value);
        }
        [HttpDelete("speakers/{id}")]
        public async Task Delete(int id)
        {
            await _service.DeleteSpeakerAsync(new DeleteSpeakerRequest {SeatId = id });
        }

        [HttpGet("discuss")]
        public async Task<ConferenceSettings> GetDiscuss()
        {
            return await _service.GetDiscuss();
        }

        [HttpPut("discuss")]
        public async Task PutDiscuss([FromBody] ConferenceSettings info)
        {
            
            await _service.PutDiscussInfo(info);
        }
        [HttpGet("voting")]
        public async Task<VotingInfo> GetVoting()
        {
            return await _service.GetVoting();
        }
        [HttpPut("voting")]
        public async Task PutVoting([FromBody] VotingInfo info)
        {
            await _service.PutVoting(info);
        }
        [HttpPut("voting/state")]
        public async Task DeleteVoting([FromBody] VotingState state)
        {
            await _service.SetStateVoting(state);
        }
        [HttpGet("voting/results")]
        public async Task<VotingResults> GetVotingResults()
        {
            return await _service.GetVotingResults();
        }
        [HttpGet("logout")]
        public void Logout()
        {
            _service.Logout();
        }
        [HttpPut("power")]
        public async Task Power([FromBody] EnergyStatus state)
        {
            await _service.SetStatus(state);
        }
    }
}
