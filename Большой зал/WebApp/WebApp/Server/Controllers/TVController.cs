﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.Server.Services;
using WebApp.Shared;

namespace WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TVController : ControllerBase
    {
        private TVService _service;
        public TVController(TVService service)
        {
            _service = service;
        }
        [HttpGet("{id}/status")]
        public async Task<int> GetStatus(string id)
        {
            return await Task.Run(() =>_service.GetState(id));
        }
        [HttpGet("{id}/on")]
        public void GetOn(string id)
        {
             _service.On(id);
        }
        [HttpGet("{id}/off")]
        public void GetOff(string id)
        {
             _service.Off(id);
        }
        // POST: api/TV
        [HttpPost]
        public void Post([FromBody] TvCredentials tv)
        {
            _service.Connect(tv);
        }
        [HttpGet("{id}/logout")]
        public void Logout(string id)
        {
            _service.Logout(id);
        }


    }
}
