﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace WebApp.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseKestrel().UseUrls(@"http://0.0.0.0:5000")
                    .UseConfiguration(new ConfigurationBuilder()
                    .AddCommandLine(args)
                    .Build())
                    .UseStartup<Startup>()
                    .Build();
    }
}
