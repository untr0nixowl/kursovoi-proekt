﻿
using SimplifiedVisca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Shared;

namespace WebApp.Server.Data
{
    public static class EntityExtensions
    {
        public static Shared.ViscaCamera MapToViscaCamera(this ScopiaFlexCamera camera, int idx)
        {
            var zoom = camera.Zoom;
            var zoomConverter = new ZoomPosition(camera.ZoomValues) { EncoderCount = (short)zoom };
            return new Shared.ViscaCamera
            {
                Id = idx,
                Name = $"Scopia Flex Camera {idx}",
                Coordinate = new Coordinates
                {
                    PanPosition = camera.CurrentPosition.PanPosition.EncoderCount,
                    TiltPosition = camera.CurrentPosition.TiltPosition.EncoderCount,
                    Zoom = zoomConverter.Ratio
                },
                Port = new PortInfo
                {
                    BaudRate = camera.Port.BaudRate,
                    Handshake = camera.Port.Handshake,
                    Parity = camera.Port.Parity,
                    PortName = camera.Port.PortName,
                    StopBits = camera.Port.StopBits
                }
            };
        }
    }
}
