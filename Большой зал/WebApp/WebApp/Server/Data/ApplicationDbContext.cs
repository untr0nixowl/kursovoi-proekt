﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Shared;

namespace WebApp.Server.Data
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) :
            base(options)
        {

        }
        public DbSet<CameraPreset> CameraPresets { get; set; }
        public DbSet<KramerDevice> Kramers { get; set; }
        public DbSet<BoschInfo> BoschInfo { get; set; }
        public DbSet<TvCredentials> TvInfoes { get; set; }

    }
}
