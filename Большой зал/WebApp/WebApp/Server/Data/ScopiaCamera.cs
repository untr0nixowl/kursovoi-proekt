﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimplifiedVisca;

namespace WebApp.Server.Data
{
    public class ScopiaCamera:ScopiaFlexCamera
    {
        public int CameraIndex { get;set; }
    }
}
