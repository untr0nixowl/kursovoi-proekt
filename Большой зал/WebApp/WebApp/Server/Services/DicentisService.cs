﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Shared;

namespace WebApp.Server.Services
{
    public class DicentisService : IDicentisService
    {
        private static readonly HttpClient _client = new HttpClient() { BaseAddress = null };
        private object _lock = new object();
        public DicentisService()
        {
            _client.DefaultRequestHeaders.ConnectionClose = false;
            _client.DefaultRequestHeaders.Connection.Add("Keep-Alive");
        }
        public void Setup(string ip)
        {
            lock (_lock)
            {
                if(_client.BaseAddress == null) { 
                    _client.BaseAddress = new Uri(ip);
                }
            }
        }
        public async Task DeleteSpeakerAsync(DeleteSpeakerRequest body)
        {
            var response = await _client.DeleteAsync($"api/speakers/{body.SeatId}");
            response.EnsureSuccessStatusCode();
        }

        public async Task<IEnumerable<SeatEntry>> GetSeatEntriesAsync()
        {
            var response = await _client.GetAsync("/api/seats");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<List<SeatEntry>>();
        }

        public async Task<IEnumerable<SpkEntry>> GetSpkEntriesAsync(bool isPolling = false)
        {
            //await _client.GetAsync("/api/keepalive");
            var response = await _client.GetAsync($"/api/speakers?isPolling={isPolling}");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<List<SpkEntry>>();
                

        }

        public async Task<IEnumerable<RtsEntry>> GetWaitersAsync()
        {
            var response = await _client.GetAsync("/api/waiting-list");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<List<RtsEntry>>();
        }
        public async Task LoginAsync(BoschInfo body)
        {
                var cred = new LoginModel { Username = body.UserName,Password = body.Password,Override = true };
                var text = JsonConvert.SerializeObject(cred);
                var content = new StringContent(text,Encoding.UTF8,"application/json");
                var response = await _client.PostAsync("/api/login",content);
                response.EnsureSuccessStatusCode();
                var answer = await response.Content.ReadAsAsync<AuthResponse>();
                lock(_lock) { 
                    _client.DefaultRequestHeaders.Add("sid", answer.Sid);
                }
        }

        public async Task PostSpeakerAsync(PostSpeakerRequest body)
        {
            var text = JsonConvert.SerializeObject(body);
            var context = new StringContent(text, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/speakers", context);
            response.EnsureSuccessStatusCode();
        }

        public async Task<ConferenceSettings> GetDiscuss()
        {
            var response = await _client.GetAsync("/api/discuss");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<ConferenceSettings>();
        }

        public async Task PutDiscussInfo(ConferenceSettings info)
        {
           var text = JsonConvert.SerializeObject(info);
           var context = new StringContent(text, Encoding.UTF8, "application/json");
           var response = await _client.PutAsync("/api/discuss", context);
           response.EnsureSuccessStatusCode();


        }

        public async Task<VotingInfo> GetVoting()
        {
            var response = await _client.GetAsync("/api/voting");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<VotingInfo>();
        }

        public async Task PutVoting(VotingInfo info)
        {
            var text = JsonConvert.SerializeObject(info);
            var context = new StringContent(text, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/voting", context);
            response.EnsureSuccessStatusCode();
        }
        public async Task SetStateVoting(VotingState state)
        {
            var text = JsonConvert.SerializeObject(state);
            var context = new StringContent(text, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/voting/state", context);
            response.EnsureSuccessStatusCode();
        }
        public async Task<VotingResults> GetVotingResults()
        {
            var response = await _client.GetAsync("/api/voting/results");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<VotingResults>();
        }
        public async Task KeepAlive()
        {
            var response = await _client.GetAsync("api/keepalive");
            response.EnsureSuccessStatusCode();
        }
        public void Logout()
        {
            
        }

        public async Task SetStatus(EnergyStatus status)
        {
            var text = JsonConvert.SerializeObject(status);
            var context = new StringContent(text, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/system/status", context);
            response.EnsureSuccessStatusCode();
        }
    }
}
