﻿using WebApp.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Server.Services
{
    public interface IKramerService
    {
        void Switch(SwitchRequest request);
    }
}
