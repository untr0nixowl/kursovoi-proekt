﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Server.Data;
using WebApp.Shared;

namespace WebApp.Server.Services
{
    public interface IViscaService
    {
        List<ScopiaCamera> Cameras { get; }
        IEnumerable<ViscaCamera> GetViscaCameras();
        ViscaCamera GetViscaCamera(string comPort);
        void AbsoluteCamera(CameraAbsolute body);
        void JogCamera(int id,CameraJoystic body);
        void CameraStop(int id);
        void CameraZoom(int id, CameraZoom zoom);
        void CameraZoomStop(int id);
    }
}
