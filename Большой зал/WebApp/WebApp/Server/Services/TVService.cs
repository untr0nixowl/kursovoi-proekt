﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using WebApp.Shared;

namespace WebApp.Server.Services
{
    public class TVService
    {
        private Dictionary<string,Socket> _sockets;
        private Dictionary<string,int> _ids;
        public TVService()
        {
            _sockets = new Dictionary<string,Socket>();
            _ids = new Dictionary<string, int>();
        }
        public void Connect(TvCredentials tv)
        {
            if(!_ids.ContainsKey(tv.Name))
            {
                var socket = new Socket(addressFamily:AddressFamily.InterNetwork,socketType:SocketType.Stream,protocolType:ProtocolType.Tcp);
                socket.Connect(tv.Ip,tv.Port);
                _sockets.Add(tv.Name,socket);
                _ids.Add(tv.Name,tv.TvId);
            }
        }
        public int GetState(string name)
        {
            Byte[] buffer = new byte[2048];
            var controlSum = 0x05 ^ ((byte)_ids[name]) ^ 0x00 ^ 0x19;
            Byte[] command = new byte[5] {0x05,(byte)_ids[name], 0x00,0x19,(byte)controlSum};
            _sockets[name].Send(command);
            var length = _sockets[name].Receive(buffer);
            return buffer[length - 2];
        }
        public void On(string name)
        {
            var controlSum = 0x06 ^ ((byte)_ids[name]) ^ 0x00 ^ 0x18 ^ 0x02;
            Byte[] command = new byte[6] { 0x06, ((byte)_ids[name]),0x00,0x18,0x02,(byte)controlSum };
            _sockets[name].Send(command);

        }
        public void Off(string name)
        {
            var controlSum = 0x06 ^ ((byte)_ids[name]) ^ 0x00 ^ 0x18 ^ 0x01;
            Byte[] command = new byte[6] { 0x06, ((byte)_ids[name]), 0x00, 0x18, 0x01, (byte)controlSum };
            _sockets[name].Send(command);
        }
        public void Logout(string id)
        {
            _sockets[id].Disconnect(true);
            _sockets.Remove(id);
            _ids.Remove(id);
        }
    }
}
