﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Shared;

namespace WebApp.Server.Services
{
    public interface IServerService
    {
        Task<List<ViscaCamera>> GetViscaCamerasAsync();
        Task<ViscaCamera> GetViscaCameraAsync(string comPort);
        Task JogCameraAsync(int id,CameraJoystic jog);
        Task AbsoluteCameraAsync(CameraAbsolute absolute);
        Task StopCameraAsync(int id);
        Task ZoomJogCameraAsync(int id,CameraZoom zoom);
        Task ZoomStopCameraAsync(int id);
        Task<List<CameraPreset>> GetCameraPresetsAsync();
        Task<List<CameraPreset>> GetCameraPresetByCom(string comPort);
        Task<List<CameraPreset>> GetCameraPreset(int id);
        Task PostCameraPreset(CameraPreset preset);
        Task PutCameraPreset(int id,CameraPreset preset);
        Task<List<KramerDevice>> GetKramerDevices();
        Task<string> Test();

    }
}
