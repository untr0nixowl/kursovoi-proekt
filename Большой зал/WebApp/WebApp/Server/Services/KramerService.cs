﻿using Kramers.Protocol;
using WebApp.Shared;

namespace WebApp.Server.Services
{
    public class KramerService
    {
        private Kramers.Protocol.Kramer3000 kramer;
        private bool isSetup;
      
        public void SetupKramer(KramerDevice dev)
        {
            if (!isSetup)
            {
                 kramer = new Kramer3000(dev.Ip, dev.Port, 2000);
                 kramer.Connect();
                 isSetup = true;
                
            }
        }

        public void Switch(SwitchRequest request)
        {
            kramer.VID(new Kramers.RequestModel.SwitcherRequest { Path = new Kramers.Models.SwitchPath { InPort = request.In, OutPort = request.Out } });
        }
        public void Logout()
        {
            isSetup = false;
        }
    }
}
