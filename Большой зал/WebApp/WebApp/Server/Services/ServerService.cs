﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebApp.Shared;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace WebApp.Server.Services
{
    public class ServerService : IServerService
    {
        private readonly HttpClient _client;
        public ServerService(HttpClient client)
        {
            _client = client;
            client.DefaultRequestHeaders.ConnectionClose = false;
            _client.DefaultRequestHeaders.Connection.Add("Keep-Alive");


        }
        public async Task AbsoluteCameraAsync(CameraAbsolute absolute)
        {
            var response = await _client.PostAsJsonAsync("/api/Cameras/movement",absolute);
            response.EnsureSuccessStatusCode();
        }

        public Task<List<CameraPreset>> GetCameraPreset(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<CameraPreset>> GetCameraPresetByCom(string comPort)
        {
            throw new NotImplementedException();
        }

        public Task<List<CameraPreset>> GetCameraPresetsAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<List<KramerDevice>> GetKramerDevices()
        {
            var response = await _client.GetAsync("/api/KramerDevices");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<List<KramerDevice>>();
        }

        public async Task<ViscaCamera> GetViscaCameraAsync(string comPort)
        {
                var response = await _client.GetAsync($"/api/Cameras/{comPort}");
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<ViscaCamera>();

        }

        public async Task<List<ViscaCamera>> GetViscaCamerasAsync()
        {
            var response = await _client.GetAsync("/api/Cameras");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<List<ViscaCamera>>();
        }

        public async Task JogCameraAsync(int id, CameraJoystic jog)
        {
            var response = await _client.PutAsJsonAsync($"/api/Cameras/movement/{id}",jog);
            response.EnsureSuccessStatusCode();
        }

        public Task PostCameraPreset(CameraPreset preset)
        {
            throw new NotImplementedException();
        }

        public Task PutCameraPreset(int id, CameraPreset preset)
        {
            throw new NotImplementedException();
        }

        public async Task StopCameraAsync(int id)
        {
            var response = await _client.PutAsync($"/api/Cameras/movement/stop/{id}",null);
            response.EnsureSuccessStatusCode();
        }

        public async Task<string> Test()
        {
            var response = await _client.GetAsync($"api/Cameras/test/0");
            response.EnsureSuccessStatusCode();
            return _client.DefaultRequestHeaders.Connection.First();
        }

        public async Task ZoomJogCameraAsync(int id, CameraZoom zoom)
        {
            var response = await _client.PutAsJsonAsync($"/api/Cameras/zoom/{id}",zoom);
            response.EnsureSuccessStatusCode();
        }

        public async Task ZoomStopCameraAsync(int id)
        {
            var response = await _client.PutAsync($"api/Cameras/zoom/stop/{id}",null);
            response.EnsureSuccessStatusCode();
        }
    }
}
