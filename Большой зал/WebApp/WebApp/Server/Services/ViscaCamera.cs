﻿using SimplifiedVisca;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Server.Data;
using WebApp.Shared;

namespace WebApp.Server.Services
{
    public class ViscaService : IViscaService
    {
        public List<ScopiaCamera> Cameras { get;set; }
        public ViscaService()
        {
            Cameras = new List<ScopiaCamera>();
            var ports = SerialPort.GetPortNames();
            var startIndex = 0;
            foreach (var port in ports)
            {
                try 
                { 
                    ScopiaCamera camera = new ScopiaCamera();
                    camera.CameraIndex = startIndex;
                    camera.Connect(port);
                    Cameras.Add(camera);
                    startIndex++;
                }
                catch
                {

                }
            }

        }

        public IEnumerable<Shared.ViscaCamera> GetViscaCameras()
        {
            return Cameras?.Select(s => s.MapToViscaCamera(s.CameraIndex));
        }

        public Shared.ViscaCamera GetViscaCamera(string comPort)
        {
            var response = Cameras?.SingleOrDefault(s => s.Port.PortName == comPort);
            return response.MapToViscaCamera(response.CameraIndex);
        }

        public void AbsoluteCamera(CameraAbsolute body)
        {
            var response = Cameras?.SingleOrDefault(s => s.Port.PortName == body.ComPort );
            if(response == null)
                return;
            response.ZoomAbsolute(body.Coordinate.Zoom);
            response.PanTiltMoveAbsolute((short)body.Coordinate.PanPosition,(short)body.Coordinate.TiltPosition);
        }

        public void JogCamera(int id, CameraJoystic body)
        {
            var response = Cameras?.SingleOrDefault(s => s.CameraIndex == id);
            if (response == null)
                return;
            response.PanTiltMove(body.Angle,body.Speed,body.IsRadiance);
        }

        public void CameraStop(int id)
        {
            var response = Cameras?.SingleOrDefault(s => s.CameraIndex == id);
            if (response == null)
                return;
            response.PanTiltStop();
        }

        public void CameraZoom(int id, CameraZoom zoom)
        {
            var response = Cameras?.SingleOrDefault(s => s.CameraIndex == id);
            if (response == null)
                return;
            response.ZoomJog(zoom.Speed,(ZOOM_DIRECTION)zoom.Direction);
        }

        public void CameraZoomStop(int id)
        {
            var response = Cameras?.SingleOrDefault(s => s.CameraIndex == id);
            if (response == null)
                return;
            response.ZoomStop();
        }
    }
}
