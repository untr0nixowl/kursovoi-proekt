﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Shared;

namespace WebApp.Server.Services
{
    public interface IDicentisService
    {
        Task LoginAsync(BoschInfo body);
        Task<IEnumerable<SeatEntry>> GetSeatEntriesAsync();
        Task<IEnumerable<SpkEntry>> GetSpkEntriesAsync(bool isPolling);
        Task<IEnumerable<RtsEntry>> GetWaitersAsync();
        Task PostSpeakerAsync(PostSpeakerRequest body);
        Task DeleteSpeakerAsync(DeleteSpeakerRequest body);
        Task<ConferenceSettings> GetDiscuss();
        Task PutDiscussInfo(ConferenceSettings info);
        Task<VotingInfo> GetVoting();
        Task PutVoting(VotingInfo info);
        Task SetStateVoting(VotingState state);
        Task<VotingResults> GetVotingResults();
        Task KeepAlive();
        void Setup(string ip);
        Task SetStatus(EnergyStatus status);
        void Logout();
    }
}
