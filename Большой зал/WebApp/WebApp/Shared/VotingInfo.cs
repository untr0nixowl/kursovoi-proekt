﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace WebApp.Shared
{
    public class VotingInfo
    {
        [JsonProperty("mode")]
        public int Mode { get; set; }
        [JsonProperty("interimResultsMode")]
        public int InterimResultsMode { get; set; }
        [JsonProperty("subject")]
        [Required]
        public string Subject { get; set; }
        [JsonProperty("individuals")]
        public bool Individuals { get; set; }
    }
}
