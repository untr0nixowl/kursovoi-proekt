﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class RtsEntry
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ParticipantId { get; set; }

        public string SeatName { get; set; }
    }
}
