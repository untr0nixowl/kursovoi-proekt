﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class AuthResponse
    {
        public int Id { get; set; }
        public string Sid { get; set; }
    }
}
