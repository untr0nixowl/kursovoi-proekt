﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebApp.Shared
{
    public class EditablePreset
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ComPort { get; set; }
        [Required]
        [Range(-180,180)]
        public int PanPosition { get; set; }
        [Required]
        [Range(-90,90)]
        public int TiltPosition { get; set; }
        [Required]
        [Range(1,18)]
        public double Zoom { get; set; }
    }
}
