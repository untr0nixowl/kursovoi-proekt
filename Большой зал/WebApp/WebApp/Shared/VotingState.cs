﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class VotingState
    {
        [JsonProperty("state")]
        public StateEnum State { get; set; }
    }
}
