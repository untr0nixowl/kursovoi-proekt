﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class DisplayTVState
    {
        public TvCredentials Tv { get; set; }
        public int State {get; set; }
    }
}
