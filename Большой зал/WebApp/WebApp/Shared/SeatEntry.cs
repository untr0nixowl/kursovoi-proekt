﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class SeatEntry
    {
        public string BatteryChanges { get; set; }
        public string BatterySerialNo { get; set; }
        public string BatteryStatus { get; set; }
        public int CameraId { get; set; }
        public int СameraPrepos { get; set; }
        public bool Connected { get; set; }
        public bool Dual { get; set; }
        public int Id { get; set; }
        public bool Identification { get; set; }
        public string Name { get; set; }
        public bool Prio { get; set; }
        public int RangeTest { get; set; }
        public bool Selected { get; set; }
        public string SignalStatus { get; set; }
        public bool Voting { get; set; }
    }
}
