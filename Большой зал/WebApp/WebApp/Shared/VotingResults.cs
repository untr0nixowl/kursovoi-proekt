﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebApp.Shared
{
    public class VotingResults
    {
        public List<VotingIndividualResults> Individuals { get; set; }
        public int Mode { get; set; }
        public List<VotingResult> Results { get; set; }
        public int State { get; set; }
        public string Subject { get; set; }
    }
}
