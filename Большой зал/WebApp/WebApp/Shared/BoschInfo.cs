﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebApp.Shared
{
    public class BoschInfo
    {
        public int Id { get; set; }

        [Url]
        [Required]
        public string Ip { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
