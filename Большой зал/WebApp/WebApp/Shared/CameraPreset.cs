﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebApp.Shared
{
    public class CameraPreset
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ComPort { get; set; }
        [Required]
        public int PanPosition { get; set; }
        [Required]
        public int TiltPosition { get; set; }
        [Required]
        public double Zoom { get; set; }
    }
}
