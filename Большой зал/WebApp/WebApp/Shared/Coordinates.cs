﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class Coordinates
    {
        public int PanPosition { get; set; }
        public int TiltPosition { get; set; }
        public double Zoom { get;set; } = 1;
    }
}
