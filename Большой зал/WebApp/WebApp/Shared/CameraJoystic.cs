﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class CameraJoystic
    {
        public int Speed { get; set; }
        public double Angle { get;set; }
        public bool IsRadiance { get; set; }
    }
}
