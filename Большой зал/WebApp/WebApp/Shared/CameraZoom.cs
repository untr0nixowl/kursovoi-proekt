﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class CameraZoom
    {
        public int Speed { get; set; }
        public int Direction { get; set; }
    }
}
