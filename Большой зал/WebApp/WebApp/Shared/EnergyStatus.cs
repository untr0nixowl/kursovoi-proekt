﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class EnergyStatus
    {
        [JsonProperty("state")]
        public int State { get; set; }
    }
}
