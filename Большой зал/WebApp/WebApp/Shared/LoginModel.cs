﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace WebApp.Shared
{
    public class LoginModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("override")]
        public bool Override { get; set; }
    }
}
