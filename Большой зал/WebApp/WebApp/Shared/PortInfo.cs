﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace WebApp.Shared
{
    public class PortInfo
    {
        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public Parity Parity { get; set; }
        public StopBits StopBits { get; set; }
        public Handshake Handshake { get; set; }
    }
}
