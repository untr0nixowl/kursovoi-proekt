﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebApp.Shared
{
    public class CameraAbsolute
    {
        public string ComPort { get; set; }
        public Coordinates Coordinate { get; set; }
    }
}
