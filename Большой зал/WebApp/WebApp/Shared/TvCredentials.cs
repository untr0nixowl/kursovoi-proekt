﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WebApp.Shared
{
    public class TvCredentials
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Ip { get; set; }
        [Required]
        public int TvId { get;set; }
        public int Port { get; set; } = 5000;
    }
}
