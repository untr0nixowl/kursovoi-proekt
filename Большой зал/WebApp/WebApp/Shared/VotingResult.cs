﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class VotingResult
    {
        public string Name { get;set; }
        public int Value { get;set; }
    }
}
