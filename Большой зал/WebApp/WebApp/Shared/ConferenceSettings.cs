﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class ConferenceSettings
    {
        [JsonProperty("maxOpenMics")]
        public int MaxOpenMics { get; set; }
        [JsonProperty("showFirstInWaitingList")]
        public bool ShowFirstInWaitingList { get; set; }
        [JsonProperty("showPossibleToSpeak")]
        public bool ShowPossibleToSpeak { get; set; }
        [JsonProperty("allowCancelRTS")]
        public bool AllowCancelRts { get; set; }
        [JsonProperty("participantMicOff")]
        public bool ParticipantMicOff { get; set; }
        [JsonProperty("priorityToneAudible")]
        public bool PriorityToneAudible { get; set; }
        [JsonProperty("priorityOption")]
        public int PriorityOption { get; set; }
        [JsonProperty("waitingListSize")]
        public int WaitingListSize { get; set; }
        [JsonProperty("autoShift")]
        public bool AutoShift { get; set; }
        [JsonProperty("ambient")]
        public bool Ambient { get; set; }
        [JsonProperty("autoMicOff")]
        public bool AutoMicOff { get; set; }
        [JsonProperty("mode")]
        public int Mode { get; set; }
    }
}
