﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class SwitchRequest
    {
        public int In { get;set; }
        public int Out { get;set; }
    }
}
