﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class SpkEntry
    {
        public int Id { get; set; }
        public bool MicOn { get; set; }
        public string Name { get; set; }
        public int ParticipantId { get; set; }
        public bool Prio { get; set; }
        public bool PrioOn { get; set; }
        public string SeatName { get; set; }
    }
}
