﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class ViscaCamera
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Coordinates Coordinate { get; set; }
        public PortInfo Port { get; set; }
    }
}
