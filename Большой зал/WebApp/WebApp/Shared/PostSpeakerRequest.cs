﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class PostSpeakerRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
