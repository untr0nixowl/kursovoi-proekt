﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class VotingIndividualResults
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParticipantId { get; set; }
        public string Result { get; set; }
        public string SeatName { get; set; }
    }
}
