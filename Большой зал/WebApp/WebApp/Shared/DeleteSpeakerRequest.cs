﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Shared
{
    public class DeleteSpeakerRequest
    {
        public int SeatId { get; set; }
    }
}
